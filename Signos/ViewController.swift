//
//  ViewController.swift
//  Signos
//
//  Created by Anna Carolina on 06/06/19.
//  Copyright © 2019 Anna Carolina. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {
    
    var signos: [String] = []
    var significadoSignos: [String] = []
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Confirgurar signos
        signos.append("Áries")
        signos.append("Touro")
        signos.append("Gemeos")
        signos.append("Cancer")
        signos.append("Leao")
        signos.append("Virgem")
        signos.append("Libra")
        signos.append("Escorpiao")
        signos.append("Sargitário")
        signos.append("Capricórnio")
        signos.append("Aquário")
        signos.append("Peixes")
        
        //Configurar Significado dos Signos
        
        significadoSignos.append("Áries é um signo de fogo, assim como Leão e Sagitário. Isso significa que está em sua natureza tomar medidas e ações, mesmo às vezes sem pensar. Seu regente fogo afeta suas excelentes habilidades organizacionais, de forma que você raramente vai encontrar um ariano que não gosta de fazer muitas coisas ao mesmo tempo, muitas vezes até mesmo antes do intervalo de almoço! Os desafios aumentam quando eles são impacientes, agressivos e descontam a sua raiva sobre os outros.")
        significadoSignos.append("Como um signo de terra, Touro pode tornar-se superprotetor de seus entes queridos. Eles são ótimos em ganhar dinheiro e seguirão com os seus projetos até que sejam concluídos com êxito.Os taurinos são muitas vezes conhecidos pela sua dureza, que também pode ser interpretada como um compromisso com a execução de tarefas. Isso os torna excelentes trabalhadores e grandes amigos, porque eles estão sempre presentes, para o que der e vier.")
        significadoSignos.append("Sendo um signo de ar, Gêmeos está preocupado com todos os aspectos da mente. Este signo é regido por Mercúrio, que é o planeta que representa a comunicação, escrita e ensinar os outros. Eles ficam fascinados por quase tudo no mundo e têm uma constante sensação de que não há tempo suficiente para experimentar tudo o que eles querem ver. Isso os torna excelentes artistas, escritores e jornalistas. O signo de Gêmeos significa que às vezes as pessoas nascidas sob este signo têm a sensação de que sua outra metade está faltando, então eles estão sempre à procura de novos amigos, mentores e colegas.")
        significadoSignos.append("Devido ao seu planeta regente ser a Lua, as várias fases do ciclo lunar podem aprofundar os mistérios internos de Câncer e criar padrões emocionais que o canceriano sensível não consegue controlar, especialmente quando criança. Isso pode mostrar-se através de alterações de humor, egoísmo, manipulação e acessos de raiva. Câncer é rápido para ajudar os outros e evitar conflitos. Um dos seus pontos mais fortes é a sua persistente determinação. Câncer não tem grandes ambições, porque eles estão felizes e contentes de ter uma família amorosa e uma casa tranquila e harmoniosa. Eles costumam cuidar bem de seus colegas de trabalho e tratá-los como família.")
        significadoSignos.append("Leão é um signo de fogo, o que significa que ele ama a vida e espera ter diversão. Como outros signos de fogo, Sagitário e Áries, Leão também é capaz de usar sua mente para resolver os problemas mais difíceis e geralmente toma a iniciativa para resolver várias situações complicadas.")
        significadoSignos.append("Virgem é um signo de Terra, preferindo as coisas conservadoras e organizadas, e os que dependem deles. As pessoas nascidas sob o signo de Virgem levam uma vida muito organizada; e mesmo que eles sejam bagunceiros, seus objetivos e sonhos estão localizados em pontos estritamente definidos em sua mente.")
        significadoSignos.append("O planeta regente de Libra é Vênus, que é um amante das coisas belas, por isso a qualidade é sempre mais importante que a quantidade para pessoas nascidas sob o signo de Libra. Eles estão sempre rodeados por arte, música e lugares bonitos. São, por natureza, cooperativos, de modo que frequentemente trabalham em equipe.")
        significadoSignos.append("Escorpião é um signo de água e vive para experimentar e expressar emoções. Embora as emoções sejam muito importantes para o Escorpião, elas se manifestam de forma diferente do que os outros signos de água. De qualquer forma, você pode ter certeza de que o Escorpião vai manter seus segredos, sejam eles quais forem.")
        significadoSignos.append("Sagitário é extrovertido, otimista e entusiasta, e gosta de mudanças. Os nascidos em Sagitário são capazes de transformar seus pensamentos em ações concretas e fazem qualquer coisa para atingir seus objetivos.")
        significadoSignos.append("Como um signo de Terra, para um capricorniano não há nada mais importante na vida do que a família. Capricórnio é um mestre em autocontrole e tem o potencial para ser um grande líder ou gestor, desde que seja na esfera dos negócios.")
        significadoSignos.append("Embora eles possam facilmente adaptar-se à energia que os rodeia, os aquarianos têm uma profunda necessidade de ficar algum tempo sozinho e longe de tudo, a fim de restabelecer a energia. As pessoas nascidas sob o signo de Aquário veem o mundo como um lugar cheio de possibilidades.")
        significadoSignos.append("Peixes é um signo de água e, dessa forma, este signo do zodíaco é caracterizado por empatia e capacidade emocional expressa.Seu planeta regente é Netuno, assim Peixes é mais intuitivo que a maioria e tem talento artístico. Netuno está ligado à música, então Peixes revela preferências musicais nos primeiros estágios da vida. Eles são generosos, compassivos e extremamente fiéis e carinhosos.As pessoas nascidas sob o signo de Peixes têm uma compreensão intuitiva do ciclo da vida e, assim, alcançam a melhor relação emocional com outros seres.")
        
   
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return signos.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let celulaReuso = "celulaReuso"
        let celula = tableView.dequeueReusableCell(withIdentifier: celulaReuso, for: indexPath)
        celula.textLabel?.text = signos [indexPath.row]
        
        return celula
        
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // desmarcar a linha
        tableView.deselectRow(at: indexPath, animated: true)
        
        //recuperar o item da lista
        
        let alertaController = UIAlertController (title: "Significado dos signos", message: significadoSignos[indexPath.row], preferredStyle: .alert)
        
        let acaoConfirmar = UIAlertAction(title: "ok", style: .default, handler: nil)
        
        alertaController.addAction(acaoConfirmar)
        
        present(alertaController, animated: true, completion: nil)
       // print(significadoSignos [ indexPath.row])
        
    }
    

}

